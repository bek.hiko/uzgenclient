import { AdminListModalComponent } from './pages/admin-list-modal/admin-list-modal.component';
import { MainComponent } from './pages/main/main.component';
import { TransportModule } from './modules/transport/transport.module';
import { ServicesModule } from './modules/services/services.module';
import { RestuarantModule } from './modules/restuarant/restuarant.module';
import { GeneralModule } from './modules/general/general.module';
import { EstateModule } from './modules/estate/estate.module';
import { ElectronicsModule } from './modules/electronics/electronics.module';
import { AnimalsModule } from './modules/animals/animals.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { WorkModule } from './modules/work/work.module';
import { HideHeaderDirective } from './directives/hide-header.directive';

@NgModule({
  declarations: [AppComponent, MainComponent,AdminListModalComponent, HideHeaderDirective],
  entryComponents: [AdminListModalComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    GeneralModule,
    AnimalsModule,
    ElectronicsModule,
    EstateModule,
    RestuarantModule,
    ServicesModule,
    TransportModule,
    WorkModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
