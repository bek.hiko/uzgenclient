import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-admin-list-modal',
  templateUrl: './admin-list-modal.component.html',
  styleUrls: ['./admin-list-modal.component.scss'],
})
export class AdminListModalComponent implements OnInit {

  constructor( private modalCtrl: ModalController) { }

  ngOnInit() {}

  dismissModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
