import { SingleElectronicComponent } from './single-electronic/single-electronic.component';
import { AllElectronicsComponent } from './all-electronics/all-electronics.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'electronics/all',
    component: AllElectronicsComponent
  },
  {
    path: 'electronics/single',
    component:SingleElectronicComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ElectronicsRoutingModule { }
