import { SingleElectronicComponent } from './single-electronic/single-electronic.component';
import { AllElectronicsComponent } from './all-electronics/all-electronics.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ElectronicsRoutingModule } from './electronics-routing.module';


@NgModule({
  declarations: [AllElectronicsComponent , SingleElectronicComponent],
  imports: [
    CommonModule,
    ElectronicsRoutingModule
  ],
  
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class ElectronicsModule { }
