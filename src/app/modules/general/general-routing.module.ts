import { SingleGeneralComponent } from './single-general/single-general.component';
import { AllGeneralComponent } from './all-general/all-general.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'general/all',
    component:AllGeneralComponent
  },
  {
    path: 'general/single',
    component: SingleGeneralComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeneralRoutingModule { }
