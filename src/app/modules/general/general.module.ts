import { SingleGeneralComponent } from './single-general/single-general.component';
import { AllGeneralComponent } from './all-general/all-general.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GeneralRoutingModule } from './general-routing.module';


@NgModule({
  declarations: [AllGeneralComponent , SingleGeneralComponent],
  imports: [
    CommonModule,
    GeneralRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class GeneralModule { }
