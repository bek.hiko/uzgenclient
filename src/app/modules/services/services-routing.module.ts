import { SingleServiceComponent } from './single-service/single-service.component';
import { AllServicesComponent } from './all-services/all-services.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'services/all',
    component: AllServicesComponent
  },
  {
    path: 'services/single',
    component: SingleServiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
