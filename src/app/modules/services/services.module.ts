import { AllServicesComponent } from './all-services/all-services.component';
import { SingleServiceComponent } from './single-service/single-service.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesRoutingModule } from './services-routing.module';


@NgModule({
  declarations: [SingleServiceComponent , AllServicesComponent],
  imports: [
    CommonModule,
    ServicesRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ServicesModule { }
