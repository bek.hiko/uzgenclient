import { SingleRestuarantsComponent } from './single-restuarants/single-restuarants.component';
import { AllRestuarantsComponent } from './all-restuarants/all-restuarants.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestuarantRoutingModule } from './restuarant-routing.module';


@NgModule({
  declarations: [AllRestuarantsComponent , SingleRestuarantsComponent],
  imports: [
    CommonModule,
    RestuarantRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RestuarantModule { }
