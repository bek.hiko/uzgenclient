import { SingleRestuarantsComponent } from './single-restuarants/single-restuarants.component';
import { AllRestuarantsComponent } from './all-restuarants/all-restuarants.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'restuarant/all',
    component: AllRestuarantsComponent
  },
  {
    path: 'restuarant/single',
    component: SingleRestuarantsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestuarantRoutingModule { }
