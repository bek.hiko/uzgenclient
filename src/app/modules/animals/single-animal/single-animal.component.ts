import { AdminListModalComponent } from './../../../pages/admin-list-modal/admin-list-modal.component';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-single-animal',
  templateUrl: './single-animal.component.html',
  styleUrls: ['./single-animal.component.scss'],
})
export class SingleAnimalComponent implements OnInit {

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  data =[
    {
      imageUrl:'/assets/svg/car.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'premium'
    },
    {
      imageUrl:'/assets/svg/animals.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/svg/food.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'premium'
    },
    {
      imageUrl:'/assets/svg/home.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/svg/team.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/svg/whatsapp.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/svg/team.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/svg/whatsapp.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    }
  ]
  
  img =['/assets/img/car.jpg','/assets/img/car1.jpg','/assets/img/car2.jpg','/assets/img/3.png']
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: AdminListModalComponent,
      cssClass: 'select-modal'
      });
    return await modal.present();
  }

}
