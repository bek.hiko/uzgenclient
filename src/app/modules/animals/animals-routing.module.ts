import { SingleAnimalComponent } from './single-animal/single-animal.component';
import { AllAnimalsComponent } from './all-animals/all-animals.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'animals/all',
    component: AllAnimalsComponent
  },
  {
    path: 'animals/single',
    component: SingleAnimalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnimalsRoutingModule { }
