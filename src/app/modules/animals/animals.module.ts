import { SingleAnimalComponent } from './single-animal/single-animal.component';
import { AllAnimalsComponent } from './all-animals/all-animals.component';
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnimalsRoutingModule } from './animals-routing.module';


@NgModule({
  declarations: [AllAnimalsComponent , SingleAnimalComponent],
  imports: [
    CommonModule,
    AnimalsRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AnimalsModule { }
