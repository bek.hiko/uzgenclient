import { SingleWorkComponent } from './single-work/single-work.component';
import { AllWorksComponent } from './all-works/all-works.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkRoutingModule } from './work-routing.module';


@NgModule({
  declarations: [AllWorksComponent , SingleWorkComponent],
  imports: [
    CommonModule,
    WorkRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class WorkModule { }
