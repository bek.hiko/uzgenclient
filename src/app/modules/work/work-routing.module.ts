import { SingleWorkComponent } from './single-work/single-work.component';
import { AllWorksComponent } from './all-works/all-works.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'work/all',
    component: AllWorksComponent
  },
  {
    path: 'work/single',
    component: SingleWorkComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkRoutingModule { }
