import { SingleTransportComponent } from './single-transport/single-transport.component';
import { AllTransportComponent } from './all-transport/all-transport.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransportRoutingModule } from './transport-routing.module';


@NgModule({
  declarations: [AllTransportComponent , SingleTransportComponent],
  imports: [
    CommonModule,
    TransportRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class TransportModule { }
