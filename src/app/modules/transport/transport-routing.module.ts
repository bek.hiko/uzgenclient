import { SingleTransportComponent } from './single-transport/single-transport.component';
import { AllTransportComponent } from './all-transport/all-transport.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'transport/all',
    component: AllTransportComponent
  },
  {
    path: 'transport/single',
    component: SingleTransportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransportRoutingModule { }
