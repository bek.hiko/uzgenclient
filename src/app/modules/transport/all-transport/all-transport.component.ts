import { Component, OnInit } from '@angular/core';
import { AdminListModalComponent } from './../../../pages/admin-list-modal/admin-list-modal.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-all-transport',
  templateUrl: './all-transport.component.html',
  styleUrls: ['./all-transport.component.scss'],
})
export class AllTransportComponent implements OnInit {
  data =[
    {
      imageUrl:'/assets/img/car.jpg',
      title: 'Купллю машину Бмв x6 2007г',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '11000$',
      status: 'premium'
    },
    {
      imageUrl:'/assets/img/room.jpg',
      title: 'Сдаеться комната для одного человека',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: 'Договорная',
      status: 'static'
    },
    {
      imageUrl:'/assets/img/phone1.jpg',
      title: 'Б/у Nokia S6 за дешево',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '100$',
      status: 'premium'
    },
    {
      imageUrl:'/assets/img/car1.jpg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/img/phone2.jpg',
      title: 'Iphone 7 за очен дешево!!!!',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/img/car.jpg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/svg/team.svg',
      title: 'Гитара Yamaha F310 ❗️В наличии❗️',
      ownerName: 'Khiko',
      liked: 100,
      views: 900,
      price: '1500$',
      status: 'static'
    },
    {
      imageUrl:'/assets/img/car2.jpg',
      title: ' Продается Bmw x6 2005г',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '10000$',
      status: 'premium'
    },
    {
      imageUrl:'/assets/img/2.jpg',
      title: 'Музыкан гитара Yamaha для ваших свадб',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: '150$',
      status: 'static'
    },
    {
      imageUrl:'/assets/img/1.png',
      title: 'Карекатуры на все вкусы',
      ownerName: 'Khiko',
      liked: 10,
      views: 9,
      price: 'Договорная',
      status: 'static'
    }
  ]

  constructor(private modalCtrl: ModalController) { }
  ngOnInit() {}
  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: AdminListModalComponent,
      cssClass: 'select-modal'
      });
    return await modal.present();
  }

}
