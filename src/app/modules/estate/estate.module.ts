import { SingleEstateComponent } from './single-estate/single-estate.component';
import { AllEstateComponent } from './all-estate/all-estate.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstateRoutingModule } from './estate-routing.module';


@NgModule({
  declarations: [AllEstateComponent,SingleEstateComponent],
  imports: [
    CommonModule,
    EstateRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class EstateModule { }
