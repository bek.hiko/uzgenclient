import { SingleEstateComponent } from './single-estate/single-estate.component';
import { AllEstateComponent } from './all-estate/all-estate.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'estate/all',
    component: AllEstateComponent
  },
  {
    path: 'estate/single',
    component: SingleEstateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstateRoutingModule { }
