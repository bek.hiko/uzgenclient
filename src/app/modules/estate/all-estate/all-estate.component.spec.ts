import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AllEstateComponent } from './all-estate.component';

describe('AllEstateComponent', () => {
  let component: AllEstateComponent;
  let fixture: ComponentFixture<AllEstateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllEstateComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AllEstateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
